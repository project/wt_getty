This module lets you use the resource labels in the Getty vocabularies
(http://vocab.getty.edu/) as a web-based taxonomy in Drupal.

Since it works as a plugin for the Web Taxonomy module
(http://drupal.org/project/web_taxonomy), that module is required too.

This code is based on jneubert's code for the dbpedia Web Taxonomy plugin.
