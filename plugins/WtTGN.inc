<?php

/**
 * @file
 * Plugin file for the Getty TGN Web Taxonomy
 */

class WtTGN extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   *
   * Uses the Getty LOD sparql endpoint.
   * For documentation see http://www.getty.edu/research/tools/vocabularies/lod/aat_semantic_representation2014-02-28.pdf.
   */
  public function autocomplete($string = '') {
    //Be kind to the Getty service and start searching at three characters
    if (strlen($string) < 3) {
      return NULL;
    }

    // Allow multiple words in the $string
    $string = trim($string); //ignore space if it is not between words
    $string = explode (' ', $string);
    foreach ($string as &$term) {
      $term = $term . '*';
    }

    $estring = $string[0];
    $i = 1;
    while ($i < count($string)) {
      $term = $string[$i];
      $estring .= ' AND ' . $term;
      $i = $i + 1;
    } 
    $string = $estring;

    // Build the query.
    $url = 'http://vocab.getty.edu/sparql.json';
    // Currently using the suggested Getty full text search query, modified to fetch results from TGN only. This will need to change for multilingual vocabularies.
    $query = 'SELECT ?Subject ?Term ?Parents ?ScopeNote ?Type { ?Subject luc:term "' . $string . '"; a ?typ. ?typ rdfs:subClassOf gvp:Subject; rdfs:label ?Type. ?Subject skos:inScheme ?vocab. ?vocab vann:preferredNamespacePrefix "tgn". optional {?Subject gvp:prefLabelGVP [skosxl:literalForm ?Term]} optional {?Subject gvp:parentStringAbbrev ?Parents} optional {?Subject skos:scopeNote [dct:language gvp_lang:en; skosxl:literalForm ?ScopeNote]}}';
   
    $params = array(
      'query' => $query,
      '_implicit' => 'false',
      'implicit' => 'true',
      '_equivalent' => 'false',
      '_form' => '/sparql',
    );

    // Get and parse the JSON query result.
    $options = array (
      'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );
    $result = drupal_http_request($url . '?' . http_build_query($params), $options);
    
    if (isset($result->error)) {
      drupal_set_message(t($result->error));
      return NULL;
    }
    $result = json_decode($result->data, TRUE);

    // Build the data structure WebTaxonomy::autocomplete() expects.
    $term_info = array();
    foreach ($result['results']['bindings'] as $binding) {
      $term_name = (string) $binding['Term']['value'];
      $uri = (string) $binding['Subject']['value'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $uri,
      );
    }
    return $term_info;
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   *
   * Reads label information from LOD representation in JSON format.
   *
   * For resource http://vocab.getty.edu/resource/tgn/7010834, the JSON file
   * is http://vocab.getty.edu/resource/tgn/7010834.json.
   */
  public function fetchTerm($term) {
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    if (!empty($web_tid)) {
      //$web_tid is something like http://vocab.getty.edu/tgn/7010834 , we need the last number
      $rev_web_tid = array_reverse(explode('/', $web_tid));
      $gvp_id = $rev_web_tid[0];
      
      // Build the query.
      $url = 'http://vocab.getty.edu/sparql.json';
      $query = 'SELECT ?literal {?subj dc:identifier "' . $gvp_id . '". ?subj skosxl:prefLabel ?label. ?label xl:literalForm ?literal.}';
   
      $params = array(
        'query' => $query,
        '_implicit' => 'false',
        'implicit' => 'true',
        '_equivalent' => 'false',
        '_form' => '/sparql',
      );
      
    }
    else {
      // @todo If there is no Web Tid, check by name.
    }

    // Get and parse the JSON query result.
    $options = array (
      'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );

    $result = drupal_http_request($url . '?' . http_build_query($params), $options);
    if (isset($result->error)) {
      drupal_set_message(t($result->error));
      return NULL;
    }
    $result = json_decode($result->data, TRUE);

    $term_info = array();
    foreach ($result['results']['bindings'] as $binding) {
      //if ($binding['literal']['xml:lang'] == 'en') { //TODO: TGN does not offer this at the JSON representation but this is where to plug other languages when the web taxonomy module and TGN allows it
        $term_name = $binding['literal']['value'];
        $term_info[$term_name] = array(
          'name' => $term_name,
          'web_tid' => $web_tid,
        ); 
      //}
    }
    return $term_info;
  }
}
