<?php

/**
 * @file
 * Plugin file for the Getty AAT Web Taxonomy
 */

class WtAAT extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   *
   * Uses the Getty LOD sparql endpoint.
   * For documentation see http://www.getty.edu/research/tools/vocabularies/lod/aat_semantic_representation2014-02-28.pdf.
   */
  public function autocomplete($string = '') {
    //Be kind to the Getty service and start searching at three characters
    if (strlen($string) < 3) {
      return NULL;
    }

    // Allow multiple words in the $string
    $string = trim($string); //ignore space if it is not between words
    $string = explode (' ', $string);
    foreach ($string as &$term) {
      $term = $term . '*';
    }

    $estring = $string[0];
    $i = 1;
    while ($i < count($string)) {
      $term = $string[$i];
      $estring .= ' AND ' . $term;
      $i = $i + 1;
    } 
    $string = $estring;

    // Build the query.
    $url = 'http://vocab.getty.edu/sparql.json';
    // See over here for explanations about this query http://answers.semanticweb.com/questions/30594/help-with-sorting.
    // TODO: this is where to plug other languages when the web taxonomy module allows it
    $query = 'SELECT ?Subject ?Term ?Parents ?ScopeNote { ?Subject luc:term "' . $string . '"; skos:inScheme aat: ; gvp:prefLabelGVP [skosxl:literalForm ?Term; gvp:term ?pureTerm]. optional {?Subject gvp:parentStringAbbrev ?Parents} optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}} ORDER BY ASC(LCASE(STR(?pureTerm)))';
   
    $params = array(
      'query' => $query,
      '_implicit' => 'false',
      'implicit' => 'true',
      '_equivalent' => 'false',
      '_form' => '/sparql',
    );

    // Get and parse the JSON query result.
    $options = array (
      'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );
    $result = drupal_http_request($url . '?' . http_build_query($params), $options);
    
    if (isset($result->error)) {
      drupal_set_message(t($result->error));
      return NULL;
    }
    $result = json_decode($result->data, TRUE);

    // Build the data structure WebTaxonomy::autocomplete() expects.
    $term_info = array();
    foreach ($result['results']['bindings'] as $binding) {
      $term_name = (string) $binding['Term']['value'];
      //some Getty terms do not have scope notes so check first
      $scope_note = isset($binding['ScopeNote']['value']) ? (string) $binding['ScopeNote']['value'] : "";
      $uri = (string) $binding['Subject']['value'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $uri,
        'description' => $scope_note,
      );
    }
    return $term_info;
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   *
   * Reads label information from LOD representation in JSON format.
   *
   * For resource http://vocab.getty.edu/resource/aat/300256003, the JSON file
   * is http://vocab.getty.edu/resource/aat/300256003.json.
   */
  public function fetchTerm($term) {
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    if (!empty($web_tid)) {
      //$web_tid is something like http://vocab.getty.edu/aat/300011851 , we need the last number
      $rev_web_tid = array_reverse(explode('/', $web_tid));
      $gvp_id = $rev_web_tid[0];
      
      // Build the query. TODO: this is where to plug other languages when the web taxonomy module allows it
      $url = 'http://vocab.getty.edu/sparql.json';
      $query = 'SELECT ?Subject ?Term ?Parents ?ScopeNote {?Subject dc:identifier "' . $gvp_id . '". ?Subject xl:prefLabel [xl:literalForm ?Term; dct:language gvp_lang:en]. optional {?Subject gvp:parentStringAbbrev ?Parents} optional {?Subject skos:scopeNote [dct:language gvp_lang:en; rdf:value ?ScopeNote]}}';
   
      $params = array(
        'query' => $query,
        '_implicit' => 'false',
        'implicit' => 'true',
        '_equivalent' => 'false',
        '_form' => '/sparql',
      );
      
    }
    else {
      // @todo If there is no Web Tid, check by name.
    }

    // Get and parse the JSON query result.
    $options = array (
      'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );

    $result = drupal_http_request($url . '?' . http_build_query($params), $options);
    if (isset($result->error)) {
      drupal_set_message(t($result->error));
      return NULL;
    }
    $result = json_decode($result->data, TRUE);

    $term_info = array();
    foreach ($result['results']['bindings'] as $binding) {
      $term_name = $binding['Term']['value'];
      $scope_note = $binding['ScopeNote']['value'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $web_tid,
        'description' => $scope_note,
      ); 
    }
    return $term_info;
  }
}
